<?php

require_once 'MagentoClient.php';

// Magento 2 instance url
$apiUrl = 'http://octo.local/index.php/rest/default/V1';

// Integration access token
$username = 'apicustomer@example.de';
$password = 'Test1234';
$token = '052u4jepyw1n6qkp40syc4u2nb644503';

// Product sku
$sku = 'OCTO-622';

// Product custom options
$customOptions = [];

// Customer address
$customerAdress = [
    'country_id' => 'DE',
    'street' => [
        0 => 'Musterstrasse 1',
    ],
    'telephone' => '123456',
    'postcode' => '70186',
    'city' => 'Stuttgart',
    'firstname' => 'Api',
    'lastname' => 'Käufer',
    'email' => 'api@mail.example',
    'prefix' => 'address_'
];

$shipTo = [
    'addressInformation' => [
        'shippingAddress' => $customerAdress,
        'billingAddress' => $customerAdress,
        'shipping_method_code' => 'freeshipping',
        'shipping_carrier_code' => 'freeshipping',
    ]
];

/**
 * Init magento api client
 */
// Use API as customer
//$magento = new MagentoClient($apiUrl, $username, $password);

// Use API as customer or admin, depending on token
$magento = new MagentoClient($apiUrl, null, null, $token);

/**
 * Api execution process
 */
$optionResult = $magento->getProductOptionByTitleIds($sku, 'color', 'cym!');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

$optionResult = $magento->getProductOptionByTitleIds($sku, 'format', 'b');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

$optionResult = $magento->getProductOptionByTitleIds($sku, 'group', 'b');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

$optionResult = $magento->getProductOptionByTitleIds($sku, 'pdf_url', 'https://www.example.com/pdf_url.pdf');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

$optionResult = $magento->getProductOptionByTitleIds($sku, 'mpp_pdf_cover_url', 'https://www.example.com/mpp_pdf_cover_url.pdf');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

$optionResult = $magento->getProductOptionByTitleIds($sku, 'mpp_pdf_innerpages_url', 'https://www.example.com/mpp_pdf_innerpages_url.pdf');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

$optionResult = $magento->getProductOptionByTitleIds($sku, 'shipping_date', 'https://www.example.com/shipping_date.pdf');
if($optionResult['success']) {
    $customOptions[] = $optionResult['result'];
} else {
    echo $optionResult['message'] . chr(10);
}

// Create cart
$cartResult = $magento->createCart();
if($cartResult['success']) {
    $cart = $cartResult['result'];
    //var_dump($cart);
} else {
    echo $cartResult['message'] . chr(10);
}

$paymentMethodsResult = $magento->getPaymentMethods($cart);
if($paymentMethodsResult['success']) {
    $paymentMethods = json_decode($paymentMethodsResult['result'], true);
    //var_dump($paymentMethods);
} else {
    echo $paymentMethodsResult['message'] . chr(10);
}

$item1Id = null;
$addToCartResult = $magento->addToCart($cart, $sku, 1, $customOptions);
if($addToCartResult['success']) {
    $item = json_decode($addToCartResult['result'], true);
    $item1Id = $item['item_id'];
    //var_dump($item);
} else {
    echo $addToCartResult['message'] . chr(10);
}

$item2Id = null;
$addToCartResult = $magento->addToCart($cart, $sku, 1, $customOptions);
if($addToCartResult['success']) {
    $item = json_decode($addToCartResult['result'], true);
    $item2Id = $item['item_id'];
    //var_dump($item);
} else {
    echo $addToCartResult['message'] . chr(10);
}

// Set shipping address
$setShippingResult = $magento->setShipping($cart, $shipTo);
if($setShippingResult['success']) {
    $orderShipment = json_decode($setShippingResult['result'], true);
    //var_dump($orderShipment);
} else {
    echo $setShippingResult['message'] . chr(10);
}

// Prepare options
$options = [
    [
        'item_id' => $item1Id,
        'trimFormat' => [
            'replace:<spoV3:trimFormat height="10" width="15"/>'
        ],
        'parameters/boundProducts/boundProduct/cover/coverPrintDatas/coverPrintData/printData' => [
            'append:{"name":"nodeD","attributes":{"attrA":"301","attrB":"D"}}',
            'prepend:{"name":"nodeE","attributes":{"attrA":"302","attrB":"E"}}',
        ],
        'parameters/boundProducts/boundProduct/cover/coverPrintDatas/coverPrintData/bleed' => [
            'after:{"name":"nodeA","attributes":{"attrA":"101","attrB":"B"}}',
            'before:{"name":"nodeB","attributes":{"attrA":"102","attrB":"C"}}'
        ],
        'padding' => [
            'remove'
        ],
        'parameters/boundProducts/boundProduct/cover' => [
            'before:{"name":"nodeC","attributes":{"attrA":"A","attrB":"200"}}'
        ],
        'result' => [
            'remove'
        ],
        'parameters/boundProducts/boundProduct/cover/BackPage' => ['remove']
    ],
    [
        'item_id' => $item2Id,
        // ...
    ]
];

$customValues = [
    [
        'item_id' => $item1Id,
        'color' => [
            'front' => ['black', 'cym'],
            'back' => []
        ],
        'format' => [
            'trim_format_width' => 111,
            'trim_format_height' => 222
        ],
        'finishing' => [
            'frontpage_name' => ['Hallo', 'Welt', '1234'],
            'backpage_name' => ['Hello', 'World', '123456']
        ],
        'paper' => [
            ['substrate' => 'Offset', 'weight' => 120, 'producer' => 'Sappi', 'surface' => 'Glänzend', 'brand' => 'Test1'],
            ['substrate' => 'Offset', 'weight' => 170, 'producer' => 'Sappi', 'surface' => 'matt', 'brand' => 'Test2'],
        ],
        'mpp_cover_color' => [
            'front' => ['c1', 'c2'],
            'back' => ['c3']
        ],
        'mpp_cover_media' => [
            'media_id' => '06424c80-5782-4b39-b63b-6394f4696251'
        ],
        'mpp_cover_type' => [
            'type' => 2
        ],
        'mpp_cover_yes_no' => [
            'is_enabled' => 1
        ],
        'mpp_format' => [
            'trim_format_width' => 123,
            'trim_format_height' => 321
        ],
        'mpp_group' => [
            [
                'signature_name' => 'signature name 1',
                'cover_signature_name' => 'cover signature name 1',
                'cover_frontpage_name' => 'cover front page name 1',
                'cover_backpage_name' => 'cover back page name 1',
                'inner_page_name' => 'inner page name 1'
            ],
            [
                'signature_name' => 'signature name 2',
                'cover_signature_name' => 'cover signature name 2',
                'cover_frontpage_name' => 'cover front page name 2',
                'cover_backpage_name' => 'cover back page name 2',
                'inner_page_name' => 'inner page name 2'
            ]
        ],
        'mpp_inner_page_color' => [
            'color_a', 'color_b', 'color_c'
        ],
        'mpp_inner_page_media' => [
            'media_id' => '06424c80-5782-4b39-b63b-6394f4696251'
        ],
        'mpp_number_of_inner_pages' => [
            'number_pages' => 10
        ],
        'production_time' => [
            'min_collection_time' => 5,
            'max_value' => 10,
            'priority_print_machine' => 15,
            'transport_deadline' => 100
        ],
        'time' => [
            [
                'quantity' => 5,
                'preparation_charge' => 10,
                'preparation_charge_type' => 'fix',
                'print_time_charge' => 15,
                'print_time_charge_type' => 'percent',
                'post_processing_charge' => 20,
                'post_processing_charge_type' => 'fix',
                'additional_drying_duration_type' => 'fix',
                'additional_drying_duration' => 5
            ],
            [
                'quantity' => 15,
                'preparation_charge' => 15,
                'preparation_charge_type' => 'fix',
                'print_time_charge' => 20,
                'print_time_charge_type' => 'percent',
                'post_processing_charge' => 25,
                'post_processing_charge_type' => 'fix',
                'additional_drying_duration_type' => 'fix',
                'additional_drying_duration' => 7
            ],
            [
                'quantity' => 50,
                'preparation_charge' => 25,
                'preparation_charge_type' => 'fix',
                'print_time_charge' => 25,
                'print_time_charge_type' => 'percent',
                'post_processing_charge' => 30,
                'post_processing_charge_type' => 'fix',
                'additional_drying_duration_type' => 'fix',
                'additional_drying_duration' => 15
            ],
        ]
    ]
];

$productData = [
    $item1Id => [
        'item_name' => 'TEST_ITEM_NAME',
        'item_row_total' => 500,
        'product_signature_margin' => '10 5'
    ]
];

// Place order
$placeOrderResult = $magento->placeOrder($cart, 'checkmo', $options, $customValues, $productData);

if($placeOrderResult['success']) {
    $orderId = $placeOrderResult['result'];
} else {
    echo $placeOrderResult['message'] . chr(10);
}

var_dump($placeOrderResult);