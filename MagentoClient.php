<?php

class MagentoClient
{
    /**
     * HTTP verb GET
     */
    const METHOD_GET = 'GET';

    /**
     * HTTP verb POST
     */
    const METHOD_POST = 'POST';

    /**
     * HTTP verb PUT
     */
    const METHOD_PUT = 'PUT';

    /**
     * HTTP verb DELETE
     */
    const METHOD_DELETE = 'DELETE';

    /**
     * Magento 2 access token
     * @var string
     */
    protected $bearerToken = null;

    /**
     * Magento 2 api base url
     * @var string
     */
    protected $baseUrl = '';

    /**
     * Magento 2 customer username (email)
     * @var string
     */
    protected $username;

    /**
     * Magento 2 customer password
     * @var string
     */
    protected $password;

    /**
     * @var int
     */
    protected $currentMode;

    /**
     * @var bool
     */

    public $debugMode = true;
    /**
     * @var array
     */
    protected $endpoints = [
        0 => [ // Admin
            self::ACTION_GET_PRODUCT => '/products/%s/', // %s = sku
            self::ACTION_GET_PRODUCT_OPTIONS => '/octo/products/%s/options', // %s = sku
            self::ACTION_CREATE_CART => '/octo/guest-carts/',
            self::ACTION_ADD_TO_CART => '/octo/guest-carts/%s/items', // %s = cart id
            self::ACTION_SET_SHIPPING => '/octo/guest-carts/%s/shipping-information', // %s = cart id
            self::ACTION_PLACE_ORDER => '/octo/order',
            self::ACTION_GET_PAYMENT_METHODS => '/octo/guest-carts/%s/payment-information', // %s = cart id
        ],
        1 => [ // Customer
            self::ACTION_GET_PRODUCT => '/products/%s/', // %s = sku
            self::ACTION_GET_PRODUCT_OPTIONS => '/octo/products/%s/options', // %s = sku
            self::ACTION_CREATE_CART => '/octo/carts/mine/',
            self::ACTION_ADD_TO_CART => '/octo/carts/mine/items',
            self::ACTION_SET_SHIPPING => '/octo/carts/mine/shipping-information',
            self::ACTION_PLACE_ORDER => '/octo/order',
            self::ACTION_GET_PAYMENT_METHODS => '/octo/carts/mine/payment-information',
        ]
    ];

    const MODE_ADMIN = 0;
    const MODE_CUSTOMER = 1;

    const ACTION_GET_PRODUCT = 'get_product';
    const ACTION_GET_PRODUCT_OPTIONS = 'get_product_options';
    const ACTION_CREATE_CART = 'create_cart';
    const ACTION_ADD_TO_CART = 'add_to_cart';
    const ACTION_SET_SHIPPING = 'set_shipping';
    const ACTION_PLACE_ORDER = 'place_order';
    const ACTION_GET_PAYMENT_METHODS = 'get_payment_methods';

    protected $productOptions = [];


    /**
     * MagentoClient constructor.
     * @param $baseUrl
     * @param string $username
     * @param string $password
     * @param string $token
     * @throws Exception
     */
    public function __construct($baseUrl, $username = null, $password = null, $token = null)
    {
        if($password === null && $token === null) {
            throw new \Exception('At least a password or a token must be provided to use the Magento API. They can not both be empty.');
        }

        if($password !== null & $token !== null) {
            throw new \Exception('Only password or token can be set at the same time.');
        }

        if($password !== null && $username === null) {
            throw new \Exception('You need to provide an username if you set a password.');
        }

        $this->baseUrl = $baseUrl;
        $this->username = $username;
        $this->password = $password;

        if($token === null) {
            $this->requestToken();
            $this->currentMode = self::MODE_CUSTOMER;
        } else {
            $this->bearerToken = $token;
            $this->currentMode = self::MODE_ADMIN;
        }
    }

    /**
     * @param string $action
     * @return string
     */
    protected function getEndpointUrl($action)
    {
        return $this->endpoints[$this->currentMode][$action];
    }

    public function requestToken()
    {
        $response = $this->request(
            '/integration/customer/token',
            self::METHOD_POST,
            json_encode([
                'username' => $this->username,
                'password' => $this->password
            ])
        );

        $response = json_decode($response, true);
        if(isset($response['message'])) {
            throw new \Exception($response['message']);
        }
        $this->bearerToken = $response;
    }

    /**
     * Execute request
     * @param string $endpoint
     * @param string $method
     * @param string|bool $body
     * @return mixed
     */
    public function request($endpoint, $method = self::METHOD_GET, $body = false)
    {
        if ($this->debugMode) echo 'Sending request to endpoint ' . $endpoint . chr(10);
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->baseUrl . $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $headers = [];
        if($this->bearerToken !== null) {
            $headers[] = 'Authorization: Bearer ' . $this->bearerToken;
        }
        if ($body) {
            $headers[] = 'Content-Type: application/json';
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    /**
     * Get product data by sku
     * @param string $sku
     * @return mixed
     * @deprecated Does not return a result as expected from NOcto API (status code, message, etc.)
     */
    public function getProduct($sku)
    {
        return $this->request(sprintf($this->getEndpointUrl(self::ACTION_GET_PRODUCT), $sku));
    }

    /**
     * Get product options with values by sku
     * @param string $sku
     * @return mixed
     */
    public function getProductOptions($sku)
    {
        if(!isset($this->productOptions[$sku])) {
            $this->productOptions[$sku] = $this->request(sprintf($this->getEndpointUrl(self::ACTION_GET_PRODUCT_OPTIONS), $sku));
        }
        return $this->productOptions[$sku];
    }

    public function getProductOptionByTitleIds($sku, $optionTitleId, $valueTitleId)
    {
        $result = json_decode($this->getProductOptions($sku), true);

        if($result['success']) {
            $productOptions = json_decode($result['result'], true);

            foreach ($productOptions as $productOption) {
                if ($productOption['title_id'] == $optionTitleId) {
                    switch ($productOption['type']) {
                        case 'drop_down':
                            foreach ($productOption['values'] as $productOptionValue) {
                                if ($productOptionValue['option_type_title_id'] == $valueTitleId) {
                                    $result['result'] = [
                                        'option_id' => $productOption['option_id'],
                                        'option_value' => $productOptionValue['option_type_id']
                                    ];
                                    return $result;
                                }
                            }
                            break;
                        case 'field':
                            $result['result'] = [
                                'option_id' => $productOption['option_id'],
                                'option_value' => $valueTitleId
                            ];
                            return $result;
                            break;
                        default:
                            $result['success'] = false;
                            $result['code'] = 207;
                            $result['message'] = 'Unknown type: ' . $productOption['type'];
                            $result['result'] = [
                                'option_id' => 0,
                                'option_value' => 0
                            ];
                            return $result;
                            break;
                    }
                }
            }

            $result['success'] = false;
            $result['message'] = 'Could not find product option by title id';
            $result['code'] = 206;
            $result['result'] = [
                'option_id' => 0,
                'option_value' => 0
            ];
        }

        return $result;
    }

    /**
     * Create a cart
     * @return string
     */
    public function createCart()
    {
        return json_decode($this->request($this->getEndpointUrl(self::ACTION_CREATE_CART), self::METHOD_POST), true);
    }

    /**
     * Add product by sku to cart. Optionally set quantity and custom options.
     * Custom options format example:
     *     $customOptions = [['option_id' => 1, 'option_value' => 5], ['option_id' => 2, 'option_value' => 11]];
     * @notice If a product custom option is necessary but is not set on api call, magento may return the error
     *         'invalid payment method'.
     * @param int $cartId
     * @param string $sku
     * @param int $quantity
     * @param array $customOptions
     * @return mixed
     */
    public function addToCart($cartId, $sku, $quantity = 1, $customOptions = [])
    {
        $order = [
            'cart_id' => $cartId,
            'cartItem' => [
                'quote_id' => $cartId,
                'sku' => $sku,
                'qty' => $quantity
            ]
        ];

        if(count($customOptions) > 0) {
            $order['cartItem']['product_option'] = [
                'extension_attributes' => [
                    'custom_options' => $customOptions
                ]
            ];
        }

        $result = $this->request(
            sprintf($this->getEndpointUrl(self::ACTION_ADD_TO_CART), $cartId),
            self::METHOD_POST,
            json_encode($order)
        );

        return json_decode($result, true);
    }

    /**
     * Set shipping information with customer adress
     * @param int $cartId
     * @param array $shipping
     * @return mixed
     */
    public function setShipping($cartId, $shipping)
    {
        if($this->currentMode == self::MODE_ADMIN) {
            $shipping['cart_id'] = $cartId;
        }

        $result = $this->request(
            sprintf($this->getEndpointUrl(self::ACTION_SET_SHIPPING), $cartId),
            self::METHOD_POST,
            json_encode($shipping)
        );

        return json_decode($result, true);
    }

    /**
     * Place the order. You can set the payment method. Also, you can receive payment methods by calling
     * getPaymentMethods($cartId). If your product price is 0, some payment methods may not be accepted.
     * @param int $cartId
     * @param string $paymentMethod
     * @param array $options
     * @return mixed
     */
    public function placeOrder($cartId, $paymentMethod = 'checkmo', $options = [], $customValues = [], $productData = [])
    {
        $body = [
            'options' => json_encode($options),
            'custom_values' => json_encode($customValues),
            'product_data' => json_encode($productData),
            'paymentMethod' => ['method' => $paymentMethod]
        ];

        if($this->currentMode == self::MODE_ADMIN) {
            $body['cart_id'] = $cartId;
        }

        $response = $this->request(
            $this->getEndpointUrl(self::ACTION_PLACE_ORDER),
            self::METHOD_PUT,
            json_encode($body)
        );

        return json_decode($response, true);
    }

    public function sendBinderySignature($orderId, $binderySignature)
    {
        // Not implemented yet
        return true;
    }

    /**
     * Get available payment methods of cart
     * @param int $cartId
     * @return mixed
     */
    public function getPaymentMethods($cartId)
    {
        $body = false;

        if($this->currentMode == self::MODE_CUSTOMER) {
            $body = json_encode(['cart_id' => $cartId]);
        }

        $result = $this->request(
            sprintf($this->getEndpointUrl(self::ACTION_GET_PAYMENT_METHODS), $cartId),
            self::METHOD_GET,
            $body
        );

        return json_decode($result, true);
    }
}